<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ProductUserScope;
use App\Helpers\Helper;


/**
 * Class Product
 *
 * @package App
 * @property string $name
 * @property text $description
 * @property decimal $price
 * @property string $photo1
 * @property string $photo2
 * @property string $photo3
*/
class Product extends Model
{
    protected $fillable = ['name', 'description', 'price', 'featured_image', 'avg_rating', 'reviews_count','affliate_link','country_id','city_id','num_of_rooms','adults','num_of_kids','latitude','longitude','service_rate','sleep_quality_rate','localtion_rate','value_rate','cleanliness_rate','rooms_rate','fitness_facilty_rate','swimming_pool_rate','published','hotel_type','minimum_stay','neighbourhood','security_deposit','hot_offer','extra_people_charge'];

    protected $hidden = [];
    public static $searchable = [
        'name',
    ];
    public $appends =['name_ar','description_ar'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ProductUserScope);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPriceAttribute($input)
    {
        $this->attributes['price'] = $input ? $input : null;
    }
    
    public function getNameArAttribute()
    {
         return  $this->attributes['name_ar'] = Helper::localization($this->getTable(),'name',$this->id,1,$this->name);
    }
    public function getDescriptionArAttribute()
    {
         return  $this->attributes['description_ar'] = Helper::localization($this->getTable(),'description',$this->id,1,$this->name);
    }
    public function category()
    {
        return $this->belongsToMany(ProductCategory::class, 'product_product_category');
    }
    
    public function tag()
    {
        return $this->belongsToMany(ProductTag::class, 'product_product_tag');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function amenties()
    {
        return $this->belongsToMany(Amenity::class, 'hotel_amenties','hotel_id','amenity_id');
    }
    public function media()
    {
        return $this->hasMany(HotelMedia::class,'hotel_id');
    }
    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }
    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }


    //Functions


    public function getHotelRate($rate)
    {
        return ($rate/5)*100;
    }


    //Hotel Serach Function
    // public static function HotelSearch($search_params , $order_by='created_at')
    // {
    //     // return static::query()->
    // }
}
