<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use App\Amenity;

class HotelsControllers extends Controller
{
    public function index(Request $request )
    {
        //List Hotels 
        // dd($request->all());
        // $hotels = Product::query()->orderBy("created_at",'DESC')->paginate(12);
        $hotels = Product::query();
        if($request->has('destination'))
        {
            $destination= $request->destination;
                $hotels->join('geo_countries','products.country_id','=','geo_countries.id')
                ->where(function ($query) use ($destination) {
                $query->where('products.name', 'LIKE','%'.$destination.'%')
                      ->orWhere('geo_countries.name', 'LIKE','%'.$destination.'%');
            });
        }
        if($request->has('price_from') && $request->has('price_to'))
        {
            $hotels->whereBetween('products.price',[$request->price_from,$request->price_to]);
        }
        if($request->has('rate')){
            $hotels->whereBetween('avg_rating',[$request->rate -1 , $request->rate]);
        }
        if($request->has('rooms')){
            $hotels->where('num_of_rooms',$request->rooms);   
        }
        if($request->has('adults')){
            $hotels->where('adults',$request->adults);
        }
        if($request->has('kids')){
            $hotels->where('num_of_kids',$request->kids);   
        }

        $hotels = $hotels->orderBy('products.created_at','DESC')->select('products.*')->paginate(12);
        // $amenties = Amenity::whereHas('hotels')->get();
        return view('frontend.pages.hotels.view',compact('hotels'));
    }
    
    public function show(Product $hotel)
    {
    	return view('frontend.pages.hotels.show',compact('hotel'));

    }

    // public function search(Request $request)
    // {
    //     $hotels_builder = Product::query();
    // }

    public function more_reviews(Request $request)
    {
    	if($request->ajax())
    	{
    		$hotel = Product::find($request->hotel_id);
    		$reviews = $hotel->reviews()->where('status_id',2)->orderBy('created_at','DESC')->paginate(3);
    		if(count($reviews) > 0)
    		{
    			$view = view('frontend.partials.more_reviews',compact('reviews'));
    			return  $view;
    		}else{

    			return '';
    		}
    		
    	}else{

    		return '';
    	}
    }
}
