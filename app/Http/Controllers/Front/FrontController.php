<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Contact;
use App\Review;
use Illuminate\Support\Facades\Mail;
use Newsletter;
class FrontController extends Controller
{
	public function index()
	{
		$hotels = Product::orderBy('avg_rating','DESC')->where('published',1)->take(12)->get();
		$hotels_count = Product::where('published',1)->count();
		$reviews = Review::orderBy('created_at','DESC')->take(3)->get();
		$hot_offers = Product::where('hot_offer',1)->where('published',1)->take(12)->get();
		return view('frontend.pages.home',compact('hotels','reviews','hot_offers','hotels_count'));
	}

	public function slider(Request $request , $id)
	{
		$hotels_media = Product::find($id)->media;
		return view('frontend.pages.hotels.slider',compact('hotels_media'));
	}

	public function aboutUs()
	{
		return view('frontend.pages.about_us');
	}
	public function ContactUs()
	{
		return view('frontend.pages.contact_us');
	}

	public function ContactUsSubmit(Request $request)
	{
			$name = $request->name;
			$email = $request->email;
			$message = $request->message;
			$save= Contact::create([
				'name' => $name,
				'email' => $email,
				'message' =>$message

			]);

		    //send email
		      Mail::raw('An Email Request for a client:'.$message, function($msg) use($email){
		          $msg->to(['info@hotelsgo.co'])->subject('Client Request');
		          $msg->from([$email]);

		        });

			return redirect()->back();
	}


	public function subscribe(Request $request)
	{
	    if ( ! Newsletter::isSubscribed($request->email) ) {
	            Newsletter::subscribe($request->email);
	        }
	        return redirect()->back();
	}

}
