<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Notifications\ReviewNotification;
use Notification;
use App\User;

class ReviewsController extends Controller
{

    /**
     * Display a listing of Reviews.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('hotel_category_access')) {
            return abort(401);
        }

        $reviews = Review::with(['product', 'user'])->orderBy('created_at','DESC')->get();

        return view('admin.reviews.index', compact('reviews'));
    }
    /**
     * Store a newly created Review in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Review::create($request->all()+['status_id'=>1]);

        //Notify Admins
        $notification_arr =[
                'message' => 'New review has been added'
        ];
        Notification::send(User::all(), new ReviewNotification($notification_arr));
        return redirect()->back();
    }

    /**
     * Remove Review from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('hotel_category_delete')) {
            return abort(401);
        }
        $review = Review::findOrFail($id);
        $review->delete();

        return redirect()->route('admin.reviews.index');
    }


    /**
    *
    */
    public function accept(Request $request , $id)
    {
        $review = Review::findOrFail($id);
        $review->status_id =2;
        $review->save();
        return redirect()->route('admin.reviews.index');


    }
    /**
    *
    */
    public function reject(Request $request , $id)
    {
        $review = Review::findOrFail($id);
        $review->status_id =3;
        $review->save();
        return redirect()->route('admin.reviews.index');

    }
    /**
     * Delete all selected Product at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('hotel_category_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Review::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
