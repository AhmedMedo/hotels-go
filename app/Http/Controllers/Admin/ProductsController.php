<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Scopes\ProductUserScope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Country;
use App\City;
use App\Helpers\Helper;
class ProductsController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('hotel_access')) {
            return abort(401);
        }


        $products = Product::all();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating new Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('hotel_create')) {
            return abort(401);
        }
        
        $categories = \App\ProductCategory::get()->pluck('name', 'id');
        $amenites = \App\Amenity::get()->pluck('name','id');

        $tags = \App\ProductTag::get()->pluck('name', 'id');
        // $countries = \App\Country::get()->pluck('name','id');

        return view('admin.products.create', compact('categories', 'tags','amenites'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductsRequest $request)
    {
        // dd($request->all());
        if (! Gate::allows('hotel_create')) {
            return abort(401);
        }

        $request = $this->saveFiles($request);

        //Save Countty and City
        $country = Country::where('name',$request->country)->first();
        if(is_null($country))
        {
            $country = new Country;
            $country->name = $request->country;
            $country->save();
        }

        $city = City::where('name',$request->city)->first();
        if(is_null($city))
        {
            $city = new City;
            $city->country_id = $country->id;
            $city->name = $request->city;
            $city->save();
        }

       // $request->request->add([
       //      'country_id' => $country->id,
       //      'city_id'    => $city->id
       //  ]);

        $product = Product::create($request->all());
        $product->country_id = $country->id;
        $product->city_id = $city->id;
        if($request->has('hot_offer'))
        {
            $hot_offer = 1;
        }else{
            $hot_offer = 0;
        }
        $product->hot_offer = $hot_offer;

        $product->save();
        // dd($product);
        $product->category()->sync(array_filter((array)$request->input('category')));
        $product->tag()->sync(array_filter((array)$request->input('tag')));
        $product->amenties()->sync(array_filter((array)$request->input('amenites')));

        //Set Media 
        if(array_key_exists('files', $request->all()))
        {
            foreach ($request->all()['files'] as $key => $value) {
                $product->media()->create(['image'=>$value]);
            }
        }

        try {
            Helper::add_localization(1,'name',$product->id,$request->name_ar,1);
            Helper::add_localization(1,'description',$product->id,$request->description_ar,1);

        } catch (\Exception $e) {
            
        }

        return redirect()->route('admin.products.index');
    }


    /**
     * Show the form for editing Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('hotel_edit')) {
            return abort(401);
        }
        
        $categories = \App\ProductCategory::get()->pluck('name', 'id');

        $tags = \App\ProductTag::get()->pluck('name', 'id');


        $product = Product::findOrFail($id);
            // dd($product->name_ar);   
        $countries = \App\Country::get()->pluck('name','id');
        $amenites = \App\Amenity::get()->pluck('name','id');

        return view('admin.products.edit', compact('product', 'categories', 'tags','amenites'));
    }

    /**
     * Update Product in storage.
     *
     * @param  \App\Http\Requests\UpdateProductsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductsRequest $request, $id)
    {
        // dd($request->all());
        if (! Gate::allows('hotel_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);

        $country = Country::where('name',$request->country)->first();
        if(is_null($country))
        {
            $country = new Country;
            $country->name = $request->country;
            $country->save();
        }

        $city = City::where('name',$request->city)->first();
        if(is_null($city))
        {
            $city = new City;
            $city->country_id = $country->id;
            $city->name = $request->city;
            $city->save();
        }
        $product = Product::findOrFail($id);
        $product->update($request->all());
        $product->country_id=$country->id;
        $product->city_id=$city->id;
        if($request->has('hot_offer'))
        {
            $hot_offer = 1;
        }else{
            $hot_offer = 0;
        }
        $product->hot_offer = $hot_offer;
        $product->save();

        //Check Old Files

        $post_media = $product->media()->pluck('id')->toArray();
        if($request->has('media_files'))
        {
            $diff = array_diff($post_media, $request->media_files);
            $product->media()->whereIn('id',$diff)->delete();

        }
        //Upload New Files 
        if(array_key_exists('files', $request->all()))
        {
            foreach ($request->all()['files'] as $key => $value) {
                $product->media()->create(['image'=>$value]);
            }
        }
        $product->category()->sync(array_filter((array)$request->input('category')));
        $product->tag()->sync(array_filter((array)$request->input('tag')));
        $product->amenties()->sync(array_filter((array)$request->input('amenites')));

        try {
            Helper::edit_entity_localization('products','name',$product->id,1,$request->name_ar);
            Helper::edit_entity_localization('products','description',$product->id,1,$request->description_ar);

        } catch (\Exception $e) {
            
        }

        return redirect()->route('admin.products.index');
    }


    /**
     * Display Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('hotel_view')) {
            return abort(401);
        }
        $product = Product::withoutGlobalScope(ProductUserScope::class)->with('reviews')->findOrFail($id);

        return view('admin.products.show', compact('product'));
    }


    /**
     * Remove Product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('hotel_delete')) {
            return abort(401);
        }
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect()->route('admin.products.index');
    }

    /**
     * Delete all selected Product at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('hotel_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Product::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function save_file(Request $request)
    {

    }

}
