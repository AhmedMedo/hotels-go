<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class NotificationController extends Controller
{
    //

    public function MarkAsRead(Request $request , $id)
    {
    	$notification = Auth::user()->notifications()->where('id',$id)->first();
    	$notification->markAsRead();
    	return redirect()->route('admin.reviews.index');
    }
}
