<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
	protected $table = 'amenites';
	protected $fillable = ['name', 'soap_icon'];
	public $timestamps = false;

	public function hotels()
	{
	    return $this->belongsToMany(Product::class, 'hotel_amenties','amenity_id','hotel_id');
	}

}
