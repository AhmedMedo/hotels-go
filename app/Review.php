<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $fillable = ['product_id', 'user_id','title', 'rating', 'comment','user_name','user_email','status_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
    	return $this->belongsTo(ReviewStatus::class,'status_id');
    }
}
