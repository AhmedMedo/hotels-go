<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMedia extends Model
{
    protected $table = 'hotels_media';
    protected $fillable =['image'];
}
