@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.reviews.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($reviews) > 0 ? 'datatable' : '' }} @can('hotel_category_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('hotel_category_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('global.reviews.fields.time')</th>
                        <th>@lang('global.reviews.fields.hotel')</th>
                        <th>@lang('global.reviews.fields.user_name')</th>
                        <th>@lang('global.reviews.fields.user_email')</th>
                        <th>@lang('global.reviews.fields.rating')</th>
                        <th>@lang('global.reviews.fields.comment')</th>
                        <th>@lang('global.reviews.fields.status')</th>
                        <th>Accept Or Reject</th>
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($reviews) > 0)
                        @foreach ($reviews as $review)
                            <tr data-entry-id="{{ $review->id }}">
                                @can('hotel_category_delete')
                                    <td></td>
                                @endcan

                                <td field-key='time'>{{ $review->created_at }}</td>
                                <td field-key='time'>{{ $review->product->name }}</td>
                                <td field-key='time'>{{ $review->user_name }}</td>
                                <td field-key='time'>{{ $review->user_email }}</td>
                                <td field-key='time'>{{ $review->rating }}</td>
                                <td field-key='time'>{{ $review->comment }}</td>
                                <td field-key='time'>{{ $review->status->name }}</td>
                                <td>
                                    @if($review->status_id == 1)
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'POST',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.reviews.accept', $review->id])) !!}
                                        {!! Form::submit(trans('global.app_accept'), array('class' => 'btn btn-xs btn-info')) !!}
                                        {!! Form::close() !!}
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'POST',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.reviews.reject', $review->id])) !!}
                                        {!! Form::submit(trans('global.app_reject'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}

                                    @elseif($review->status_id == 2)
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'POST',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.reviews.reject', $review->id])) !!}
                                        {!! Form::submit(trans('global.app_reject'), array('class' => 'btn btn-xs btn-success')) !!}
                                        {!! Form::close() !!}

                                    @elseif($review->status_id == 3)
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'POST',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.reviews.accept', $review->id])) !!}
                                        {!! Form::submit(trans('global.app_accept'), array('class' => 'btn btn-xs btn-info')) !!}
                                        {!! Form::close() !!}


                                    @endif
                                </td>
                                <td>
                                    @can('hotel_category_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.reviews.destroy', $review->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('hotel_category_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.reviews.mass_destroy') }}';
        @endcan

    </script>
@endsection