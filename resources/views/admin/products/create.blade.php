@extends('layouts.app')
@section('styles')
        <link rel="stylesheet" href="{{asset('js/dropify/dropify.css')}}">

<style type="text/css">
     #map {
        height: 600px;
      }
       #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
      .btn {
          display: inline-block;
          padding: 6px 12px;
          margin-bottom: 0;
          font-size: 14px;
          font-weight: normal;
          line-height: 1.42857143;
          text-align: center;
          white-space: nowrap;
          vertical-align: middle;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
          background-image: none;
          border: 1px solid transparent;
          border-radius: 4px;
      }
      /*Also */
      .btn-success {
          border: 1px solid #c5dbec;
          background: #D0E5F5;
          font-weight: bold;
          color: #2e6e9e;
      }
      /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
      .fileinput-button {
          position: relative;
          overflow: hidden;
      }

          .fileinput-button input {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              opacity: 0;
              -ms-filter: 'alpha(opacity=0)';
              font-size: 200px;
              direction: ltr;
              cursor: pointer;
          }

      .thumb {
          height: 80px;
          width: 100px;
          border: 1px solid #000;
      }

      ul.thumb-Images li {
          width: 120px;
          float: left;
          display: inline-block;
          vertical-align: top;
          height: 120px;
      }

      .img-wrap {
          position: relative;
          display: inline-block;
          font-size: 0;
      }

          .img-wrap .close {
              position: absolute;
              top: 2px;
              right: 2px;
              z-index: 100;
              background-color: #D0E5F5;
              padding: 5px 2px 2px;
              color: #000;
              font-weight: bolder;
              cursor: pointer;
              opacity: .5;
              font-size: 23px;
              line-height: 10px;
              border-radius: 50%;
          }

          .img-wrap:hover .close {
              opacity: 1;
              background-color: #ff0000;
          }

      .FileNameCaptionStyle {
          font-size: 12px;
      }
</style>
@endsection
@section('content')
    <h3 class="page-title">@lang('global.hotels.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.products.store'], 'files' => true,]) !!}
        <div class="row">
          <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.app_create')
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 form-group">
                            {!! Form::label('name', trans('global.hotels.fields.name').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-6 form-group">
                            {!! Form::label('name', trans('global.hotels.fields.name_ar').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('name_ar', old('name_ar'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('name_ar'))
                                <p class="help-block">
                                    {{ $errors->first('name_ar') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('description', trans('global.hotels.fields.description').'', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('description'))
                                <p class="help-block">
                                    {{ $errors->first('description') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('description', trans('global.hotels.fields.description_ar').'', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description_ar', old('description_ar'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('description_ar'))
                                <p class="help-block">
                                    {{ $errors->first('description_ar') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 form-group">
                            {!! Form::label('price', trans('global.hotels.fields.price').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('price', old('price'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('price'))
                                <p class="help-block">
                                    {{ $errors->first('price') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-6 form-group">
                            {!! Form::label('affliate link', trans('global.hotels.fields.affliate_link').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('affliate_link', old('affliate_link'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('affliate_link'))
                                <p class="help-block">
                                    {{ $errors->first('affliate_link') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 form-group">
                            {!! Form::label('num of kids', trans('global.hotels.fields.num_of_kids').'*', ['class' => 'control-label']) !!}
                            {!! Form::number('num_of_kids', old('num_of_kids'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('num_of_kids'))
                                <p class="help-block">
                                    {{ $errors->first('num_of_kids') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-4 form-group">
                            {!! Form::label('Number of Rooms', trans('global.hotels.fields.num_of_rooms').'*', ['class' => 'control-label']) !!}
                            {!! Form::number('num_of_rooms', old('number_of_rooms'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('number_of_rooms'))
                                <p class="help-block">
                                    {{ $errors->first('number_of_rooms') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-4 form-group">
                            {!! Form::label('Adults', trans('global.hotels.fields.adults').'*', ['class' => 'control-label']) !!}
                            {!! Form::number('adults', old('adults'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('adults'))
                                <p class="help-block">
                                    {{ $errors->first('adults') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('amenites', trans('global.hotels.fields.amenites').'', ['class' => 'control-label']) !!}
                            <button type="button" class="btn btn-primary btn-xs" id="selectbtn-amenites">
                                {{ trans('global.app_select_all') }}
                            </button>
                            <button type="button" class="btn btn-primary btn-xs" id="deselectbtn-amenites">
                                {{ trans('global.app_deselect_all') }}
                            </button>
                            {!! Form::select('amenites[]', $amenites, old('amenites'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'id' => 'selectall-amenites' ]) !!}
                            <p class="help-block"></p>
                            @if($errors->has('amenites'))
                                <p class="help-block">
                                    {{ $errors->first('amenites') }}
                                </p>
                            @endif
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <span class="btn btn-success fileinput-button">
                                <span>Upload Media</span>
                                <input type="file" name="files[]" id="files" multiple accept="image/jpeg, image/png, image/gif,"><br />
                            </span>
                            <output id="Filelist"></output>
                         </div>
                     </div>              
                 
                    <div class="row">
                          <div class="col-md-12 form-group">
                              <label class="control-label">Choose Hotel Location:</label>
                                 <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                  <div id="map">
                                 </div>
                                <input type="hidden" name="country" id="country">
                                <input type="hidden" name="city" id="city">
                                <input type="hidden" name="longitude" id="lng">
                                <input type="hidden" name="latitude" id="lat">
                        </div>
                    </div>                  
                </div>
            </div>
          </div>
          <div class="col-md-3">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                         <h3 class="box-title">Publish and Tags</h3>

                    <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                    </div>
                        <!-- /.box-tools -->
                    </div>
                      <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Published', trans('global.hotels.fields.published'), ['class' => 'control-label']) !!}
                                {!! Form::select('published',['1'=>'Published','0'=>'Draft'],old('published'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('published'))
                                    <p class="help-block">
                                        {{ $errors->first('published') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('category', trans('global.hotels.fields.category').'', ['class' => 'control-label']) !!}
                                <button type="button" class="btn btn-primary btn-xs" id="selectbtn-category">
                                    {{ trans('global.app_select_all') }}
                                </button>
                                <button type="button" class="btn btn-primary btn-xs" id="deselectbtn-category">
                                    {{ trans('global.app_deselect_all') }}
                                </button>
                                {!! Form::select('category[]', $categories, old('category'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'id' => 'selectall-category' ]) !!}
                                <p class="help-block"></p>
                                @if($errors->has('category'))
                                    <p class="help-block">
                                        {{ $errors->first('category') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('tag', trans('global.hotels.fields.tag').'', ['class' => 'control-label']) !!}
                                <button type="button" class="btn btn-primary btn-xs" id="selectbtn-tag">
                                    {{ trans('global.app_select_all') }}
                                </button>
                                <button type="button" class="btn btn-primary btn-xs" id="deselectbtn-tag">
                                    {{ trans('global.app_deselect_all') }}
                                </button>
                                {!! Form::select('tag[]', $tags, old('tag'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'id' => 'selectall-tag' ]) !!}
                                <p class="help-block"></p>
                                @if($errors->has('tag'))
                                    <p class="help-block">
                                        {{ $errors->first('tag') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('tag', trans('global.hotels.fields.hotel_offer').'', ['class' => 'control-label']) !!}
                                {!!  Form::checkbox('hot_offer', '1'); !!}
                            </div>
                        </div>
                     </div>
                      <!-- /.box-body -->
                </div>
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                         <h3 class="box-title">Rating</h3>

                    <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                    </div>
                        <!-- /.box-tools -->
                    </div>
                      <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Service Rate', trans('global.hotels.fields.service_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('service_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('service_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('service_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('service_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Sleep Quality Rate', trans('global.hotels.fields.sleep_quality_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('sleep_quality_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('sleep_quality_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('sleep_quality_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('sleep_quality_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Location Rate', trans('global.hotels.fields.localtion_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('localtion_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('localtion_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('localtion_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('localtion_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Swimming Pool Rate', trans('global.hotels.fields.swimming_pool_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('swimming_pool_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('swimming_pool_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('swimming_pool_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('swimming_pool_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Value Rate', trans('global.hotels.fields.value_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('value_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('value_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('value_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('value_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Cleanliness Rate', trans('global.hotels.fields.cleanliness_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('cleanliness_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('cleanliness_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('cleanliness_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('cleanliness_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Rooms Rate', trans('global.hotels.fields.rooms_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('rooms_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('rooms_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('rooms_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('rooms_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Fitness Facilty Rate', trans('global.hotels.fields.fitness_facilty_rate'), ['class' => 'control-label']) !!}
                                {!! Form::select('fitness_facilty_rate',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('fitness_facilty_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('fitness_facilty_rate'))
                                    <p class="help-block">
                                        {{ $errors->first('fitness_facilty_rate') }}
                                    </p>
                                @endif
                            </div>
                        </div>




                     </div>
                      <!-- /.box-body -->
                </div>
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                         <h3 class="box-title">Extra Info</h3>

                    <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                    </div>
                        <!-- /.box-tools -->
                    </div>
                      <!-- /.box-header -->
                    <div class="box-body" style="">
                         <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Hotel Type', trans('global.hotels.fields.hotel_type').' * Stars *', ['class' => 'control-label']) !!}
                                {!! Form::select('hotel_type',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('service_rate'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('hotel_type'))
                                    <p class="help-block">
                                        {{ $errors->first('hotel_type') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Extra People', trans('global.hotels.fields.extra_people_charge'), ['class' => 'control-label']) !!}
                                {!! Form::select('extra_people_charge',['charge'=>'charge','No Charge'=>'No Charge'],old('extra_people_charge'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('extra_people_charge'))
                                    <p class="help-block">
                                        {{ $errors->first('extra_people_charge') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-xs-12">
                                {!! Form::label('Minimum Stay', trans('global.hotels.fields.minimum_stay').' * Nights *', ['class' => 'control-label']) !!}
                                {!! Form::select('minimum_stay',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],old('minimum_stay'),['class'=>'form-control']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('minimum_stay'))
                                    <p class="help-block">
                                        {{ $errors->first('minimum_stay') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-xs-12">
                                 {!! Form::label('Security Deposit', trans('global.hotels.fields.security_deposit'), ['class' => 'control-label']) !!}
                                {!! Form::text('security_deposit', old('security_deposit'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}

                                 <p class="help-block"></p>
                                 @if($errors->has('security_deposit'))
                                     <p class="help-block">
                                         {{ $errors->first('security_deposit') }}
                                     </p>
                                 @endif
                             </div>
                         </div>
                     </div>
                      <!-- /.box-body -->
                </div>
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                         <h3 class="box-title">Featured Image</h3>

                    <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                    </div>
                        <!-- /.box-tools -->
                    </div>
                      <!-- /.box-header -->
                    <div class="box-body" style="">
                         <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::file('featured_image', ['class' => 'form-control dropify','data-plugin'=>'dropify','data-allowed-file-extensions'=>'png jpg jpeg']) !!}
                                {!! Form::hidden('featured_image_max_size', 8) !!}
                                {!! Form::hidden('featured_image_max_width', 300) !!}
                                {!! Form::hidden('featured_image_max_height', 300) !!}
                                <p class="help-block"></p>
                                @if($errors->has('featured_image'))
                                    <p class="help-block">
                                        {{ $errors->first('featured_image') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                     </div>
                      <!-- /.box-body -->
                </div>
                    <!-- /.box -->
           </div>
        </div>
    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script>
        $("#selectbtn-category").click(function(){
            $("#selectall-category > option").prop("selected","selected");
            $("#selectall-category").trigger("change");
        });
        $("#deselectbtn-category").click(function(){
            $("#selectall-category > option").prop("selected","");
            $("#selectall-category").trigger("change");
        });

    </script>
    <script>
        $("#selectbtn-amenites").click(function(){
            $("#selectall-amenites > option").prop("selected","selected");
            $("#selectall-amenites").trigger("change");
        });
        $("#deselectbtn-amenites").click(function(){
            $("#selectall-amenites > option").prop("selected","");
            $("#selectall-amenites").trigger("change");
        });
        
    </script>

    <script>
        $("#selectbtn-tag").click(function(){
            $("#selectall-tag > option").prop("selected","selected");
            $("#selectall-tag").trigger("change");
        });
        $("#deselectbtn-tag").click(function(){
            $("#selectall-tag > option").prop("selected","");
            $("#selectall-tag").trigger("change");
        });
    </script>

    <script type="text/javascript">
        //google Maps
          function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -33.8688, lng: 151.2195},
              zoom: 13,
              mapTypeId: 'roadmap'
            });
          service = new google.maps.places.PlacesService(map);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
              searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
              var places = searchBox.getPlaces();

              if (places.length == 0) {
                return;
              }

              if(places.length > 1 ){
                alert("You have to choose single place");
                $('#submit').prop('disabled', true);
              }else{
                $('#submit').prop('disabled', false);
                    // Clear out the old markers.
                    markers.forEach(function(marker) {
                      marker.setMap(null);
                    });
                    markers = [];

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function(place) {
                      if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                      }
                      var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                      };

                      // Create a marker for each place.
                      markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                      }));
                      
                      if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                      } else {
                        bounds.extend(place.geometry.location);
                      }
                        service.getDetails(place, function(result, status) {
                          if (status !== google.maps.places.PlacesServiceStatus.OK) {
                              console.error(status);
                              return;
                          }
                            var data = result.address_components;
                            var city='';
                            var country ='';

                            console.log(data);
                            data.forEach((element, index) => {
                                // console.log(element.types); // 100, 200, 300
                                // console.log(element.long_name); // 100, 200, 300
                                // console.log(element.short_name); // 100, 200, 300
                                var type = element.types;
                                if(type.includes('country'))
                                {
                                    country = element.long_name;
                                }
                                if(type.includes('locality'))
                                {
                                    city = element.long_name;
                                }else{
                                    if(type.includes('administrative_area_level_2'))
                                    {
                                        city = element.long_name;
                                    }
                                    else if(type.includes('administrative_area_level_1')){
                                        city = element.long_name;
                                    }


                                }

                                // console.log(index); // 0, 1, 2
                            });

                            console.log(city);
                            console.log(country);
                           // console.log(result.address_components);
                          $("#country").val(country);
                          $("#city").val(city);
                          $("#lat").val(result.geometry.location.lat());
                          $("#lng").val(result.geometry.location.lng());
                      });

                    });
                    map.fitBounds(bounds);            
              }
                 



            });
          }

    </script>
     <script src="{{asset('js/dropify/dropify.min.js')}}"></script>

    <script type="text/javascript">
         $('.dropify').dropify();


    </script>
    <script type="text/javascript">

        //I added event handler for the file upload control to access the files properties.
        document.addEventListener("DOMContentLoaded", init, false);

        //To save an array of attachments 
        var AttachmentArray = [];

        //counter for attachment array
        var arrCounter = 0;

        //to make sure the error message for number of files will be shown only one time.
        var filesCounterAlertStatus = false;

        //un ordered list to keep attachments thumbnails
        var ul = document.createElement('ul');
        ul.className = ("thumb-Images");
        ul.id = "imgList";

        function init() {
            //add javascript handlers for the file upload event
            document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        }

        //the handler for file upload event
        function handleFileSelect(e) {
            //to make sure the user select file/files
            if (!e.target.files) return;

            //To obtaine a File reference
            var files = e.target.files;

            // Loop through the FileList and then to render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {

                //instantiate a FileReader object to read its contents into memory
                var fileReader = new FileReader();

                // Closure to capture the file information and apply validation.
                fileReader.onload = (function (readerEvt) {
                    return function (e) {
                        
                        //Apply the validation rules for attachments upload
                        ApplyFileValidationRules(readerEvt)

                        //Render attachments thumbnails.
                        RenderThumbnail(e, readerEvt);

                        //Fill the array of attachment
                        FillAttachmentArray(e, readerEvt)
                    };
                })(f);

                // Read in the image file as a data URL.
                // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
                // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                fileReader.readAsDataURL(f);
            }
            document.getElementById('files').addEventListener('change', handleFileSelect, false);
        }

        //To remove attachment once user click on x button
        jQuery(function ($) {
            $('div').on('click', '.img-wrap .close', function () {
                var id = $(this).closest('.img-wrap').find('img').data('id');

                //to remove the deleted item from array
                var elementPos = AttachmentArray.map(function (x) { return x.FileName; }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                //to remove image tag
                $(this).parent().find('img').not().remove();

                //to remove div tag that contain the image
                $(this).parent().find('div').not().remove();

                //to remove div tag that contain caption name
                $(this).parent().parent().find('div').not().remove();

                //to remove li tag
                var lis = document.querySelectorAll('#imgList li');
                for (var i = 0; li = lis[i]; i++) {
                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }

            });
        }
        )

        //Apply the validation rules for attachments upload
        function ApplyFileValidationRules(readerEvt)
        {
            //To check file type according to upload conditions
            if (CheckFileType(readerEvt.type) == false) {
                alert("The file (" + readerEvt.name + ") does not match the upload conditions, You can only upload jpg/png/gif files");
                e.preventDefault();
                return;
            }

            //To check file Size according to upload conditions
            if (CheckFileSize(readerEvt.size) == false) {
                alert("The file (" + readerEvt.name + ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB");
                e.preventDefault();
                return;
            }

            //To check files count according to upload conditions
            if (CheckFilesCount(AttachmentArray) == false) {
                if (!filesCounterAlertStatus) {
                    filesCounterAlertStatus = true;
                    alert("You have added more than 10 files. According to upload conditions you can upload 10 files maximum");
                }
                e.preventDefault();
                return;
            }
        }

        //To check file type according to upload conditions
        function CheckFileType(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            }
            if (fileType == "image/jpg") {
                return true;
            }
            else if (fileType == "image/png") {
                return true;
            }
            else if (fileType == "image/gif") {
                return true;
            }
            else {
                return false;
            }
            return true;
        }

        //To check file Size according to upload conditions
        function CheckFileSize(fileSize) {
            if (fileSize < 1000000) {
                return true;
            }
            else {
                return false;
            }
            return true;
        }

        //To check files count according to upload conditions
        function CheckFilesCount(AttachmentArray) {
            //Since AttachmentArray.length return the next available index in the array, 
            //I have used the loop to get the real length
            var len = 0;
            for (var i = 0; i < AttachmentArray.length; i++) {
                if (AttachmentArray[i] !== undefined) {
                    len++;
                }
            }
            //To check the length does not exceed 10 files maximum
            if (len > 9) {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Render attachments thumbnails.
        function RenderThumbnail(e, readerEvt)
        {
            var li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = ['<div class="img-wrap"> <span class="close">&times;</span>' +
                '<img class="thumb" src="', e.target.result, '" title="', escape(readerEvt.name), '" data-id="',
                readerEvt.name, '"/>' + '</div>'].join('');

            var div = document.createElement('div');
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join('');
            document.getElementById('Filelist').insertBefore(ul, null);
        }

        //Fill the array of attachment
        function FillAttachmentArray(e, readerEvt)
        {
            AttachmentArray[arrCounter] =
            {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size,
            };
            arrCounter = arrCounter + 1;
        }
    </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0fOlryatxjNt1vQMdT0oKxaR9HI8zww&libraries=places&callback=initAutocomplete"></script>

@stop