    <title>Hotels GO</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="B-verify" content="37abb06a2fb72858005a1d642fcec806de2a2388" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
     @if(App::getLocale()=="ar")
     <link rel="stylesheet" href="{{asset('frontend/css/bootstrap-rtl.min.css')}}">
     @endif
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('frontend/css/animate.min.css')}}">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/components/revolution_slider/css/settings.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/components/revolution_slider/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/components/jquery.bxslider/jquery.bxslider.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/components/flexslider/flexslider.css')}}" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/updates.css')}}">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/custom.css')}}">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">

    @if(App::getLocale()=="ar")
    <link rel="stylesheet" href="{{asset('frontend/css/rtl.css')}}">
    @endif

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <!-- Javascript Page Loader -->
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/4d8a4ea7ac463caa7213ae32f/e3b23def9bd788ed506e2602c.js");</script>

@yield('styles')