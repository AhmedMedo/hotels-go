    <script type="text/javascript" src="{{asset('frontend/js/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.noconflict.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/modernizr.2.7.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.placeholder.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery-ui.1.10.4.min.js')}}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{asset('frontend/components/revolution_slider/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/components/revolution_slider/js/jquery.themepunch.revolution.min.js')}}"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="{{asset('frontend/components/jquery.bxslider/jquery.bxslider.min.js')}}"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="{{asset('frontend/components/flexslider/jquery.flexslider.js')}}"></script>

    <!-- parallax -->
    <script type="text/javascript" src="{{asset('frontend/js/jquery.stellar.min.js')}}"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="{{asset('frontend/js/jquery.stellar.min.js')}}"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="{{asset('frontend/js/waypoints.min.js')}}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{asset('frontend/js/theme-scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/scripts.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

    <script type="text/javascript">

    	//Add Class Active 
    	// var url = window.location;
    	// console.log(url);


    	// // for sidebar menu entirely but not cover treeview
    	// tjq('ul.menu a').filter(function() {
    	//     return this.href == url;
    	// }).parent().addClass('active');

        tjq(document).ready(function() {
            tjq('.revolution-slider').revolution(
            {
                sliderType:"standard",
				sliderLayout:"auto",
				dottedOverlay:"none",
				delay:9000,
				navigation: {
					keyboardNavigation:"off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation:"off",
					mouseScrollReverse:"default",
					onHoverStop:"on",
					touch:{
						touchenabled:"on",
						swipe_threshold: 75,
						swipe_min_touches: 1,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					}
					,
					arrows: {
						style:"default",
						enable:true,
						hide_onmobile:false,
						hide_onleave:false,
						tmp:'',
						left: {
							h_align:"left",
							v_align:"center",
							h_offset:20,
							v_offset:0
						},
						right: {
							h_align:"right",
							v_align:"center",
							h_offset:20,
							v_offset:0
						}
					}
				},
				visibilityLevels:[1240,1024,778,480],
				gridwidth:1170,
				gridheight:646,
				lazyType:"none",
				shadow:0,
				spinner:"spinner4",
				stopLoop:"off",
				stopAfterLoops:-1,
				stopAtSlide:-1,
				shuffle:"off",
				autoHeight:"off",
				hideThumbsOnMobile:"off",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
            });
        });
    </script>
@yield('scripts')