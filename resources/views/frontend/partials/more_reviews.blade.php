    @foreach($reviews as $review)
<div class="guest-review table-wrapper">
    <div class="col-xs-3 col-md-2 author table-cell">
        <a href="#"><img src="http://placehold.it/270x263" alt="" width="270" height="263" /></a>
        <p class="name">{{$review->user_name}}</p>
        <p class="date">{{$review->created_at->format('F')}}, {{$review->created_at->format('d')}}, {{$review->created_at->format('Y')}}</p>
    </div>
    <div class="col-xs-9 col-md-10 table-cell comment-container">
        <div class="comment-header clearfix">
            <h4 class="comment-title">{{$review->title}}</h4>
            <div class="review-score">
                <div class="five-stars-container"><div class="five-stars" style="width: {{($review->rating/5) * 100}}%;"></div></div>
                <span class="score">{{$review->rating}}/5.0</span>
            </div>
        </div>
        <div class="comment-content">
            <p>{{$review->comment}}</p>
        </div>
    </div>
</div>
@endforeach