@extends('frontend.layout.main')
@section('styles')
    
@endsection
@section('content')
<div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">About Us</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{route('homepage')}}">HOME</a></li>
                    <li class="active">About Us</li>
                </ul>
            </div>
        </div>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="large-block image-box style6">
                <article class="box">
                    <figure class="col-md-5">
                        <a href="#" title="" class="middle-block"><img class="middle-item" src="{{asset('frontend/images/who_we.jpg')}}" alt="" width="476" height="318" /></a>
                    </figure>
                    <div class="details col-md-offset-5">
                        <h4 class="box-title">@lang('about.who_we_are')</h4>
                        <p>@lang('about.who_we_are_section')</p>
                    </div>
                </article>
                <article class="box">
                    <figure class="col-md-5 pull-right middle-block">
                        <a href="#" title=""><img class="middle-item" src="{{asset('frontend/images/we_do.jpg')}}" alt="" width="476" height="318" /></a>
                    </figure>
                    <div class="details col-md-7">
                        <h4 class="box-title">@lang('about.what_we_do')</h4>
                        <p>@lang('about.what_we_do_section')</p>
                    </div>
                </article>
                <article class="box">
                    <figure class="col-md-5">
                        <a href="#" title="" class="middle-block"><img class="middle-item" src="{{asset('frontend/images/works.jpg')}}" alt="" /></a>
                    </figure>
                    <div class="details col-md-offset-5">
                        <h4 class="box-title">@lang('about.how_works')</h4>
                        <p>@lang('about.how_works_section')</p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>


@endsection