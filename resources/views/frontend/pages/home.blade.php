@extends('frontend.layout.main')
@section('styles')
    <script type="text/javascript" src="{{asset('frontend/js/pace.min.js')}}" data-pace-options='{ "ajax": false }'></script>
    <script type="text/javascript" src="{{asset('frontend/js/page-loading.js')}}"></script>
    <style type="text/css">
        .five-stars-container.editable-rating .ui-slider-range {
            display: block;
            overflow: hidden;
            position: relative;
            background: none;
            padding-left: 1px;
        }
        .overall-rating{
            padding: 0;
        }
        #price-filter{
            margin-top: 20px;
        }
    </style>
@endsection
@section('content')
<div class="banner imagebg-container" style="height: 450px; background-image: url('{{asset('frontend/images/hotel.jpg')}}'); background-size:cover">
    <div class="container">
        <h1 class="big-caption">@lang('global.home.banner.title')<em><strong>@lang('global.home.banner.bold')</strong></em> @lang('global.menu.hotels')!</h1>
        <h2 class="med-caption">@lang('global.home.banner.title_two')</h2>
    </div>
</div>

<div class="page-title-container style3">
    <div class="container">
        <form method="GET" action="{{route('hotels.list')}}">
            <div class="row">
                <div class="form-group col-sm-6 col-md-3">
                    <label>@lang('home.search.your_destination')</label>
                    <input type="text" name="destination" class="input-text full-width" placeholder="@lang('home.search.text_placeholder')">
                </div>
                <div class="form-group col-sms-6 col-sm-3 col-md-2">
                    <label>@lang('home.search.price')</label>
                    <div id="price-filter">
                        <div class="panel-content">
                            <div id="price-range"></div>
                            <br />
                            @if(App::getLocale()=="en")
                            <span class="min-price-label pull-left"></span>
                            <span class="max-price-label pull-right"></span>
                            @else
                            <span class="min-price-label pull-right"></span>
                            <span class="max-price-label pull-left"></span>
                            @endif

                            <div class="clearer"></div>
                            <input type="hidden" name="price_from" value="0" id="price_from">
                            <input type="hidden" name="price_to" value="1000" id="price_to">

                        </div><!-- end content -->
                    </div>
                </div>
                <div class="form-group col-sms-6 col-sm-3 col-md-2">
                    <label>@lang('home.search.rate')</label>
                    <div class="selector">
                        <select name="rate">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                        </select>
                    </div>
<!--                     <div class="overall-rating">
                        <div class="star-rating clearfix">
                            <div class="each-rating">
                                <div class="five-stars-container editable-rating" data-original-stars="4">  
                                </div>
                                <input type="hidden" name="rate" value="4" id="rate">
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="form-group col-xs-4 col-sm-2 col-md-1">
                    <label>@lang('home.search.rooms')</label>
                    <div class="selector">
                        <select name="rooms">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-4 col-sm-2 col-md-1">
                    <label>@lang('home.search.adults')</label>
                    <div class="selector">
                        <select name="adults">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="4">05</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-4 col-sm-2 col-md-1">
                    <label>@lang('home.search.kids')</label>
                    <div class="selector">
                        <select name="kids">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-6 col-md-2">
                    <label>&nbsp;</label>
                    <button class="full-width icon-check">@lang('home.search.search_now')</button>
                </div>
            </div>
        </form>
    </div>
</div>

        <section id="content">
            <div class="section">
                <div class="container">
                    <h2>@lang('home.recommended_hotels')</h2>
                    <div class="row image-box style1 add-clearfix">
                            @foreach($hotels as $hotel)
                            <div class="col-sms-6 col-sm-6 col-md-3">
                                <article class="box">
                                    <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1">
                                            <a href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}" alt="" width="270" height="160" /></a>
                                        </figure>
                                    <div class="details">
                                        <span class="price"><small>FROM</small>${{$hotel->price}}</span>
                                        <h4 class="box-title"><a href="{{route('hotels.show',$hotel)}}">Atlantis - The Palm<small>Paris</small></a></h4>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                    </div>
                </div>
                <div class="global-map-area section parallax" data-stellar-background-ratio="0.5" style="margin-top: 100px;">
                    <div class="container">
                        <h1 class="text-center white-color">@lang('home.top_reviewers')</h1>
                        <div class="testimonial style3">
                            <ul class="slides ">
                                @foreach($reviews as $review)
                                <li>
                                    <div class="author"><a href="#"><img src="http://placehold.it/270x270" alt="" width="74" height="74" /></a></div>
                                    <blockquote class="description">{{$review->comment}}.</blockquote>
                                    <h2 class="name">{{$review->user_name}}</h2>
                                </li>
                                @endforeach
                             
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="section">  
                <div class="container">                 
                    <h2>@lang('home.hot_offers')</h2>
                    <div class="row image-box style1 add-clearfix">
                            @foreach($hot_offers as $hotel)
                            <div class="col-sms-6 col-sm-6 col-md-3">
                                <article class="box has-discount">
                                    <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1">
                                            <a href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}" alt="" width="270" height="160" /></a>
                                        </figure>
                                    <div class="details">
                                        <span class="price"><small>FROM</small>${{$hotel->price}}</span>
                                        <h4 class="box-title"><a href="{{route('hotels.show',$hotel)}}">Atlantis - The Palm<small>Paris</small></a></h4>
                                        <span class="discount"><span class="discount-text"> @lang('home.hot_offer') </span></span>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                    </div>
              </div>
           </div>
            <div class="global-map-area promo-box no-margin parallax" data-stellar-background-ratio="0.5">
                <div class="container">
                    <div class="content-section description pull-right col-sm-9">
                        <div class="table-wrapper hidden-table-sm">
                            <div class="table-cell">
                                <h2 class="m-title animated" data-animation-type="fadeInDown">
                                    @lang('home.subscribe.subscribe_title')<br /><em>{{$hotels_count}}+ @lang('home.subscribe.hotels_and_resorts')</em>
                                </h2>
                            </div>
                            <div class="table-cell action-section col-md-4 no-float">
                                <form action="{{route('subscribe')}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-xs-6 col-md-12">
                                            <input type="text" name="email" class="input-text input-large full-width" value="" placeholder="@lang('home.subscribe.email_placeholder')" />
                                        </div>
                                        <div class="col-xs-6 col-md-12">
                                            <button class="full-width btn-large animated" data-animation-type="fadeInUp" data-animation-delay="1">@lang('home.subscribe.button')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="image-container col-sm-3">
                        <img src="{{asset('frontend/images/test_two.png')}}" alt="" width="342" height="258" class="animated" data-animation-type="fadeInUp" data-animation-duration="2" />
                    </div>
                </div>
            </div>
        </section>

@endsection

@section('scripts')

<script type="text/javascript">
    tjq("#price-range").slider({
        range: true,
        min: 0,
        max: 1000,
        values: [ 0, 1000 ],
        slide: function( event, ui ) {
            tjq(".min-price-label").html( "$" + ui.values[ 0 ]);
            tjq(".max-price-label").html( "$" + ui.values[ 1 ]);
            tjq("#price_from").val(ui.values[ 0 ]);
            tjq("#price_to").val(ui.values[ 1 ]);
        }
    });
    tjq(".min-price-label").html( "$" + tjq("#price-range").slider( "values", 0 ));
    tjq(".max-price-label").html( "$" + tjq("#price-range").slider( "values", 1 ));
    tjq(".editable-rating.five-stars-container").each(function() {
        var oringnal_value = tjq(this).data("original-stars");
        if (typeof oringnal_value == "undefined") {
            oringnal_value = 0;
        } else {
            //oringnal_value = 10 * parseInt(oringnal_value);
        }

        tjq(this).slider({
            range: "min",
            value: oringnal_value,
            min: 0,
            max: 5,
            slide: function( event, ui ) {
                console.log(ui.value);
                tjq("#rate").val(ui.value);

            }
        });
    });
</script>
@endsection