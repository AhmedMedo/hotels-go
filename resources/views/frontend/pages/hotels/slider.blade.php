<div class="photo-gallery style1" id="photo-gallery1" data-animation="slide" data-sync="#image-carousel1">
    <ul class="slides">
        @foreach($hotels_media as $media)
        <li><img src="{{asset(env('UPLOAD_PATH'))}}/{{$media->image}}" width="900" height="500" alt="" /></li>
        @endforeach
<!--         <li><img src="http://placehold.it/900x500" alt="" /></li>
 -->    </ul>
</div>
<div class="image-carousel style1" id="image-carousel1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photo-gallery1">
    <ul class="slides">
        @foreach($hotels_media as $media)
        <li><img src="{{asset(env('UPLOAD_PATH'))}}/thumb/{{$media->image}}" alt="" /></li>
        @endforeach
<!--         <li><img src="http://placehold.it/70x70" alt="" /></li>
 -->
    </ul>
</div>