@extends('frontend.layout.main')
@section('styles')
    
@endsection
@section('content')
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Hotel Search Results</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="#">HOME</a></li>
            <li class="active">Hotel Search Results</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="row">
                <div class="col-sm-12 col-md-12" id="hotels_list">
                    <div class="sort-by-section clearfix">
                        <h4 class="sort-by-title block-sm">Sort results by:</h4>
                        <ul class="sort-bar clearfix block-sm">
                            <li class="sort-by-name"><a class="sort-by-container sort" data-sort="name"><span>name</span></a></li>
                            <li class="sort-by-price"><a class="sort-by-container sort" data-sort="price"><span>price</span></a></li>
                            <li class="clearer visible-sms"></li>
                            <li class="sort-by-rating"><a class="sort-by-container sort" data-sort="rating"><span>rating</span></a></li>
                        </ul>
                        
                          <!--   <ul class="swap-tiles clearfix block-sm">
                                <li class="swap-list {{request()->segment(2) =='list' ? 'active':''}}">
                                    <a href="{{route('hotels.list','list')}}"><i class="soap-icon-list"></i></a>
                                </li>
                                <li class="swap-grid {{request()->segment(2) =='grid' ? 'active':''}}">
                                    <a href="{{route('hotels.list','grid')}}"><i class="soap-icon-grid"></i></a>
                                </li>
                                <li class="swap-block {{request()->segment(2) =='block' ? 'active':''}}">
                                    <a href="{{route('hotels.list','block')}}"><i class="soap-icon-block"></i></a>
                                </li>
                            </ul> -->
                    </div>
                    <div class="hotel-list">
                            <div class="row image-box hotel listing-style1">
                                <ul class="list">
                                @if(count($hotels))
                                @foreach($hotels as $hotel)
                                <li>
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                                <a title="" href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img width="270" height="160" alt="" src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}"></a>
                                            </figure>
                                            <div class="details">
                                                <span class="price">
                                                    <small>avg/night</small>
                                                    ${{$hotel->price}}
                                                </span>
                                                <h4 class="box-title name">{{$hotel->name}}<small>{{$hotel->country->name}} {{$hotel->city->name}}</small></h4>
                                                <div class="feedback">
                                                    <div data-placement="bottom" data-toggle="tooltip" class="five-stars-container" title="4 stars"><span style="width: {{$hotel->getHotelRate($hotel->avg_rating)}}%;" class="five-stars"></span></div>
                                                    <div class="rating" style="display: none;">{{$hotel->avg_rating}} </div>

                                                    <span class="review">{{$hotel->reviews_count}} reviews</span>
                                                </div>
                                                <p class="description">{{ substr($hotel->description,0,20)}}</p>
                                                <div class="action">
                                                    <a class="button btn-small" href="{{route('hotels.show',$hotel)}}">SELECT</a>
                                                    <a class="button btn-small yellow popup-map" href="#" data-box="{{$hotel->latitude}}, {{$hotel->longitude}}">VIEW ON MAP</a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </li>
                                @endforeach
                                @endif
                                </ul>

                            </div>
                    </div>                    <a href="#" class="uppercase full-width button btn-large">load more listing</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0fOlryatxjNt1vQMdT0oKxaR9HI8zww&libraries=places&callback=initMap"></script>

<script type="text/javascript" src="{{asset('frontend/js/gmap3.min.js')}}"></script>

<script type="text/javascript">
    tjq(document).ready(function() {
        tjq("#price-range").slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 100, 800 ],
            slide: function( event, ui ) {
                tjq(".min-price-label").html( "$" + ui.values[ 0 ]);
                tjq(".max-price-label").html( "$" + ui.values[ 1 ]);
            }
        });
        tjq(".min-price-label").html( "$" + tjq("#price-range").slider( "values", 0 ));
        tjq(".max-price-label").html( "$" + tjq("#price-range").slider( "values", 1 ));
        
        tjq("#rating").slider({
            range: "min",
            value: 40,
            min: 0,
            max: 50,
            slide: function( event, ui ) {
                
            }
        });
    });


    var options = {
      valueNames: [ 'name','price','rating']
    };

    var userList = new List('hotels_list', options);

</script>

@endsection

