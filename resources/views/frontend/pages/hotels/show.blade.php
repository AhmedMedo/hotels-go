@extends('frontend.layout.main')
@section("styles")
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 600px;
      }

    </style>


@endsection
@section('content')

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">@lang('hotel.hotel_deatiled')</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="#">HOME</a></li>
            <li class="active">Hotel Detailed</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="tab-container style1" id="hotel-main-content">
                    <ul class="tabs">
                        <li class="active"><a data-toggle="tab" href="#photos-tab">@lang('hotel.photos')</a></li>
                        <li><a data-toggle="tab" href="#map-tab">@lang('hotel.map')</a></li>
                        <!-- <li><a data-toggle="tab" href="#steet-view-tab">street view</a></li>
                        <li><a data-toggle="tab" href="#calendar-tab">calendar</a></li> -->
                        <li class="pull-right"><a class="button btn-small yellow-bg white-color" href="{{$hotel->affliate_link}}">@lang('hotel.view')</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="photos-tab" class="tab-pane fade in active">
                        	@if(count($hotel->media))
                        	<div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                        	    <ul class="slides">
                        	    	@foreach($hotel->media as $media)
                        	    	<li><img src="{{asset(env('UPLOAD_PATH'))}}/{{$media->image}}" width="900" height="500" alt="" /></li>
                        	    	@endforeach
                        	    </ul>
                        	</div>
                        	<div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                        	    <ul class="slides">
                        	    	@foreach($hotel->media as $media)
                        	    	<li><img src="{{asset(env('UPLOAD_PATH'))}}/thumb/{{$media->image}}" alt="" /></li>
                        	    	@endforeach
                        	    </ul>
                        	</div>
                        	@else
                        	<div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                        	    <ul class="slides">
                        	        <li><img src="http://placehold.it/900x500" alt="" /></li>
                        	    </ul>
                        	</div>
                        	<div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                        	    <ul class="slides">
                        	        <li><img src="http://placehold.it/70x70" alt="" /></li>
                        	    </ul>
                        	</div>
                        	@endif


                        </div>
                        <div id="map-tab" class="tab-pane fade">
                            <div id="map">
                            </div>

                        </div>
                        <div id="steet-view-tab" class="tab-pane fade" style="height: 500px;">
                            
                        </div>
                        <!-- <div id="calendar-tab" class="tab-pane fade">
                            <label>SELECT MONTH</label>
                            <div class="col-sm-6 col-md-4 no-float no-padding">
                                <div class="selector">
                                    <select class="full-width" id="select-month">
                                        <option value="2014-6">June 2014</option>
                                        <option value="2014-7">July 2014</option>
                                        <option value="2014-8">August 2014</option>
                                        <option value="2014-9">September 2014</option>
                                        <option value="2014-10">October 2014</option>
                                        <option value="2014-11">November 2014</option>
                                        <option value="2014-12">December 2014</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="calendar"></div>
                                    <div class="calendar-legend">
                                        <label class="available">available</label>
                                        <label class="unavailable">unavailable</label>
                                        <label class="past">past</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <p class="description">
                                        The calendar is updated every five minutes and is only an approximation of availability.
						        <br /><br />
						        Some hosts set custom pricing for certain days on their calendar, like weekends or holidays. The rates listed are per day and do not include any cleaning fee or rates for extra people the host may have for this listing. Please refer to the listing's Description tab for more details.
						        <br /><br />
						        We suggest that you contact the host to confirm availability and rates before submitting a reservation request.
                                    </p>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div id="hotel-features" class="tab-container">
                    <ul class="tabs">
                        <li class="active"><a href="#hotel-description" data-toggle="tab">@lang('hotel.description')</a></li>
<!--                        <li><a href="#hotel-availability" data-toggle="tab">Availability</a></li>
 -->                         <li><a href="#hotel-amenities" data-toggle="tab">@lang('hotel.amenities')</a></li>
                        <li><a href="#hotel-reviews" data-toggle="tab">@lang('hotel.reviews')</a></li>
<!--                         <li><a href="#hotel-faqs" data-toggle="tab">FAQs</a></li>
                        <li><a href="#hotel-things-todo" data-toggle="tab">Things to Do</a></li>
 -->                        <li><a href="#hotel-write-review" data-toggle="tab">@lang('hotel.write_review')</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="hotel-description">
                            <div class="intro table-wrapper full-width hidden-table-sms">
                                <div class="col-sm-5 col-lg-4 features table-cell">
                                    <ul>
                                        <li><label>@lang('hotel.description_section.hotel_type'):</label>{{$hotel->hotel_type}} star</li>
                                        <li><label>@lang('hotel.description_section.extra_people'):</label>{{$hotel->extra_people_charge}}</li>
                                        <li><label>@lang('hotel.description_section.minimum_stay'):</label>{{$hotel->minimum_stay}} nights</li>
                                        <li><label>@lang('hotel.description_section.security_deposit'):</label>${{$hotel->security_deposit}}</li>
                                        <li><label>@lang('hotel.description_section.country'):</label>{{$hotel->country->name}}</li>
                                        <li><label>@lang('hotel.description_section.city'):</label>{{$hotel->city->name}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                    <div class="testimonial style1">
                                        @if(count($hotel->reviews))
                                        <ul class="slides ">
                                            @foreach($hotel->reviews()->where('status_id',2)->orderBy('created_at','DESC')->take(3)->get() as $review)
                                            <li>
                                                <p class="description">{{$review->comment}}</p>
                                                <div class="author clearfix">
                                                    <a href="#"><img src="http://placehold.it/270x270" alt="" width="74" height="74" /></a>
                                                    <h5 class="name">{{$review->user_name}}<small>guest</small></h5>
                                                </div>
                                            </li>
                                            @endforeach
                                        
                                        </ul>
                                        @else

                                        No Reviews
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="long-description">
                                <h2>About {{$hotel->name}}</h2>
                                    {!! $hotel->description !!}
                            </div>
                        </div>
                        <!-- <div class="tab-pane fade" id="hotel-availability">
                            <form>
                            <div class="update-search clearfix">
                                <div class="col-md-5">
                                    <h4 class="title">When</h4>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label>CHECK IN</label>
                                            <div class="datepicker-wrap">
                                                    <input type="text" name="date_from" placeholder="mm/dd/yy" class="input-text full-width" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>CHECK OUT</label>
                                            <div class="datepicker-wrap">
                                                    <input type="text" name="date_to" placeholder="mm/dd/yy" class="input-text full-width" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <h4 class="title">Who</h4>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label>ROOMS</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>ADULTS</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>KIDS</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <h4 class="visible-md visible-lg">&nbsp;</h4>
                                    <label class="visible-md visible-lg">&nbsp;</label>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button data-animation-duration="1" data-animation-type="bounce" class="full-width icon-check animated" type="submit">SEARCH NOW</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <h2>Available Rooms</h2>
                            <div class="room-list listing-style3 hotel">
                                <article class="box">
                                    <figure class="col-sm-4 col-md-3">
                                        <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.html" title=""><img width="230" height="160" src="http://placehold.it/230x160" alt=""></a>
                                    </figure>
                                    <div class="details col-xs-12 col-sm-8 col-md-9">
                                        <div>
                                            <div>
                                                <div class="box-title">
                                                    <h4 class="title">Standard Family Room</h4>
                                                    <dl class="description">
                                                        <dt>Max Guests:</dt>
                                                        <dd>3 persons</dd>
                                                    </dl>
                                                </div>
                                                <div class="amenities">
                                                    <i class="soap-icon-wifi circle"></i>
                                                    <i class="soap-icon-fitnessfacility circle"></i>
                                                    <i class="soap-icon-fork circle"></i>
                                                    <i class="soap-icon-television circle"></i>
                                                </div>
                                            </div>
                                            <div class="price-section">
                                                <span class="price"><small>PER/NIGHT</small>$121</span>
                                            </div>
                                        </div>
                                        <div>
                                            <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                            <div class="action-section">
                                                <a href="hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure class="col-sm-4 col-md-3">
                                        <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.html" title=""><img width="230" height="160" src="http://placehold.it/230x160" alt=""></a>
                                    </figure>
                                    <div class="details col-xs-12 col-sm-8 col-md-9">
                                        <div>
                                            <div>
                                                <div class="box-title">
                                                    <h4 class="title">Superior Double Room</h4>
                                                    <dl class="description">
                                                        <dt>Max Guests:</dt>
                                                        <dd>5 persons</dd>
                                                    </dl>
                                                </div>
                                                <div class="amenities">
                                                    <i class="soap-icon-wifi circle"></i>
                                                    <i class="soap-icon-fitnessfacility circle"></i>
                                                    <i class="soap-icon-fork circle"></i>
                                                    <i class="soap-icon-television circle"></i>
                                                </div>
                                            </div>
                                            <div class="price-section">
                                                <span class="price"><small>PER/NIGHT</small>$241</span>
                                            </div>
                                        </div>
                                        <div>
                                            <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                            <div class="action-section">
                                                <a href="hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure class="col-sm-4 col-md-3">
                                        <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.html" title=""><img width="230" height="160" src="http://placehold.it/230x160" alt=""></a>
                                    </figure>
                                    <div class="details col-xs-12 col-sm-8 col-md-9">
                                        <div>
                                            <div>
                                                <div class="box-title">
                                                    <h4 class="title">Deluxe Single Room</h4>
                                                    <dl class="description">
                                                        <dt>Max Guests:</dt>
                                                        <dd>4 persons</dd>
                                                    </dl>
                                                </div>
                                                <div class="amenities">
                                                    <i class="soap-icon-wifi circle"></i>
                                                    <i class="soap-icon-fitnessfacility circle"></i>
                                                    <i class="soap-icon-fork circle"></i>
                                                    <i class="soap-icon-television circle"></i>
                                                </div>
                                            </div>
                                            <div class="price-section">
                                                <span class="price"><small>PER/NIGHT</small>$137</span>
                                            </div>
                                        </div>
                                        <div>
                                            <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                            <div class="action-section">
                                                <a href="hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure class="col-sm-4 col-md-3">
                                        <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.html" title=""><img width="230" height="160" src="http://placehold.it/230x160" alt=""></a>
                                    </figure>
                                    <div class="details col-xs-12 col-sm-8 col-md-9">
                                        <div>
                                            <div>
                                                <div class="box-title">
                                                    <h4 class="title">Single Bed Room</h4>
                                                    <dl class="description">
                                                        <dt>Max Guests:</dt>
                                                        <dd>2 persons</dd>
                                                    </dl>
                                                </div>
                                                <div class="amenities">
                                                    <i class="soap-icon-wifi circle"></i>
                                                    <i class="soap-icon-fitnessfacility circle"></i>
                                                    <i class="soap-icon-fork circle"></i>
                                                    <i class="soap-icon-television circle"></i>
                                                </div>
                                            </div>
                                            <div class="price-section">
                                                <span class="price"><small>PER/NIGHT</small>$55</span>
                                            </div>
                                        </div>
                                        <div>
                                            <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                            <div class="action-section">
                                                <a href="hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <a href="#" class="load-more button full-width btn-large fourty-space">LOAD MORE ROOMS</a>
                            </div>
                            
                        </div -->
                        <div class="tab-pane fade" id="hotel-amenities">
                             <ul class="amenities clearfix style1">
                                @foreach($hotel->amenties as $amenty)
                                <li class="col-md-4 col-sm-6">
                                    <div class="icon-box style1"><i class="{{$amenty->soap_icon}}"></i>{{$amenty->name}}</div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="hotel-reviews">
                            <div class="intro table-wrapper full-width hidden-table-sms">
                                <div class="rating table-cell col-sm-4">
                                    <span class="score">{{$hotel->avg_rating}}/5.0</span>
                                    <div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->avg_rating)}}%;"></div></div>
<!--                                     <a href="#" class="goto-writereview-pane button green btn-small full-width">WRITE A REVIEW</a>
 -->                                </div>
                                <div class="table-cell col-sm-8">
                                    <div class="detailed-rating">
                                        <ul class="clearfix">
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.service')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->service_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.value')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->value_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.sleep_quality')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->sleep_quality_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.cleanliness')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->cleanliness_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.location')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->localtion_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.rooms')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->rooms_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.swimming_pool')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->swimming_pool_rate)}}%;"></div></div></div></li>
                                            <li class="col-md-6"><div class="each-rating"><label>@lang('hotel.reviews_section.fitness_facilty')</label><div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($hotel->fitness_facilty_rate)}}%;"></div></div></div></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="guest-reviews">
                                <h2>@lang('hotel.guest_reviews')</h2>
                                @if(count($hotel->reviews))
                                    @foreach($hotel->reviews()->where('status_id',2)->orderBy('created_at','DESC')->paginate(3) as $review)
                                <div class="guest-review table-wrapper">
                                    <div class="col-xs-3 col-md-2 author table-cell">
                                        <a href="#"><img src="http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                        <p class="name">{{$review->user_name}}</p>
                                        <p class="date">{{$review->created_at->format('F')}}, {{$review->created_at->format('d')}}, {{$review->created_at->format('Y')}}</p>
                                    </div>
                                    <div class="col-xs-9 col-md-10 table-cell comment-container">
                                        <div class="comment-header clearfix">
                                            <h4 class="comment-title">{{$review->title}}</h4>
                                            <div class="review-score">
                                                <div class="five-stars-container"><div class="five-stars" style="width: {{$hotel->getHotelRate($review->rating)}}%;"></div></div>
                                                <span class="score">{{$review->rating}}/5.0</span>
                                            </div>
                                        </div>
                                        <div class="comment-content">
                                            <p>{{$review->comment}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                    No Reviews

                                @endif
                    
                            </div>
                            <a id="load_more_reviews" class="button full-width btn-large">LOAD MORE REVIEWS</a>
                        </div>
<!--                         <div class="tab-pane fade" id="hotel-faqs">
                            <h2>Frquently Asked Questions</h2>
                            <div class="topics">
                                <ul class="check-square clearfix">
                                    <li class="col-sm-6 col-md-4"><a href="#">address &amp; map</a></li>
                                    <li class="col-sm-6 col-md-4"><a href="#">messaging</a></li>
                                    <li class="col-sm-6 col-md-4"><a href="#">refunds</a></li>
                                    <li class="col-sm-6 col-md-4"><a href="#">pricing</a></li>
                                    <li class="col-sm-6 col-md-4 active"><a href="#">reservation requests</a></li>
                                    <li class="col-sm-6 col-md-4"><a href="#">your reservation</a></li>
                                </ul>
                            </div>
                            <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                            <div class="toggle-container">
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question1" data-toggle="collapse">How do I know a reservation is accepted or confirmed?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question1">
                                        <div class="panel-content">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question2" data-toggle="collapse">What do I do after I receive a reservation request from a guest?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question2">
                                        <div class="panel-content">
                                            <p>Sed a justo enim. Vivamus volutpat ipsum ultrices augue porta lacinia. Proin in elementum enim. <span class="skin-color">Duis suscipit justo</span> non purus consequat molestie. Etiam pharetra ipsum sagittis sollicitudin ultricies. Praesent luctus, diam ut tempus aliquam, diam ante euismod risus, euismod viverra quam quam eget turpis. Nam <span class="skin-color">tristique congue</span> arcu, id bibendum diam. Ut hendrerit, leo a pellentesque porttitor, purus arcu tristique erat, in faucibus elit leo in turpis vitae luctus enim, a mollis nulla.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="" href="#question3" data-toggle="collapse">How much time do I have to respond to a reservation request?</a>
                                    </h4>
                                    <div class="panel-collapse collapse in" id="question3">
                                        <div class="panel-content">
                                            <p>Sed a justo enim. Vivamus volutpat ipsum ultrices augue porta lacinia. Proin in elementum enim. <span class="skin-color">Duis suscipit justo</span> non purus consequat molestie. Etiam pharetra ipsum sagittis sollicitudin ultricies. Praesent luctus, diam ut tempus aliquam, diam ante euismod risus, euismod viverra quam quam eget turpis. Nam <span class="skin-color">tristique congue</span> arcu, id bibendum diam. Ut hendrerit, leo a pellentesque porttitor, purus arcu tristique erat, in faucibus elit leo in turpis vitae luctus enim, a mollis nulla.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question4" data-toggle="collapse">Why can’t I call or email hotel or host before booking?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question4">
                                        <div class="panel-content">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question5" data-toggle="collapse">Am I allowed to decline reservation requests?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question5">
                                        <div class="panel-content">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question6" data-toggle="collapse">What happens if I let a reservation request expire?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question6">
                                        <div class="panel-content">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#question7" data-toggle="collapse">How do I set reservation requirements?</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="question7">
                                        <div class="panel-content">
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="hotel-things-todo">
                            <h2>Things to Do</h2>
                            <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                            <div class="activities image-box style2 innerstyle">
                                <article class="box">
                                    <figure>
                                        <a title="" href="#"><img width="250" height="161" alt="" src="http://placehold.it/250x160"></a>
                                    </figure>
                                    <div class="details">
                                        <div class="details-header">
                                            <div class="review-score">
                                                <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                <span class="reviews">25 reviews</span>
                                            </div>
                                            <h4 class="box-title">Central Park Walking Tour</h4>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                        <a class="button" title="" href="#">MORE</a>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure>
                                        <a title="" href="#"><img width="250" height="161" alt="" src="http://placehold.it/250x160"></a>
                                    </figure>
                                    <div class="details">
                                        <div class="details-header">
                                            <div class="review-score">
                                                <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                <span class="reviews">25 reviews</span>
                                            </div>
                                            <h4 class="box-title">Museum of Modern Art</h4>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                        <a class="button" title="" href="#">MORE</a>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure>
                                        <a title="" href="#"><img width="250" height="161" alt="" src="http://placehold.it/250x160"></a>
                                    </figure>
                                    <div class="details">
                                        <div class="details-header">
                                            <div class="review-score">
                                                <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                <span class="reviews">25 reviews</span>
                                            </div>
                                            <h4 class="box-title">Crazy Horse Cabaret Show</h4>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                        <a class="button" title="" href="#">MORE</a>
                                    </div>
                                </article>
                            </div>
                        </div>
 -->                        <div class="tab-pane fade" id="hotel-write-review">
<!--                             <div class="main-rating table-wrapper full-width hidden-table-sms intro">
                                <article class="image-box box hotel listing-style1 photo table-cell col-sm-4">
                                    <figure>
                                        <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="http://placehold.it/270x160"></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Hilton Hotel and Resorts<small><i class="soap-icon-departure"></i> Paris, france</small></h4>
                                        <div class="feedback">
                                            <div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: 80%;"></span></div>
                                            <span class="review">270 reviews</span>
                                        </div>
                                    </div>
                                </article>
                                <div class="table-cell col-sm-8">
                                    <div class="overall-rating">
                                        <h4>Your overall Rating of this property</h4>
                                        <div class="star-rating clearfix">
                                            <div class="five-stars-container"><div class="five-stars" style="width: 80%;"></div></div>
                                            <span class="status">VERY GOOD</span>
                                        </div>
                                        <div class="detailed-rating">
                                            <ul class="clearfix">
                                                <li class="col-md-6"><div class="each-rating"><label>service</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>Value</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>Sleep Quality</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>Cleanliness</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>location</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>rooms</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>swimming pool</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                                <li class="col-md-6"><div class="each-rating"><label>fitness facility</label><div class="five-stars-container editable-rating" data-original-stars="4"></div></div></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
 -->                            <form class="review-form" method="POST" action="{{route('reviews.store')}}">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{$hotel->id}}">
                                <div class="form-group col-md-5 no-float no-padding">
                                    <h4 class="title">@lang('hotel.write_review_section.title')</h4>
                                    <input type="text" name="title" class="input-text full-width" value="" placeholder="enter a review title" />
                                </div>
                                <div class="form-group col-md-5 no-float no-padding">
                                    <h4 class="title">@lang('hotel.write_review_section.name')</h4>
                                    <input type="text" name="user_name" class="input-text full-width" value="" placeholder="enter your name" />
                                </div>
                                <div class="form-group col-md-5 no-float no-padding">
                                    <h4 class="title">@lang('hotel.write_review_section.email')</h4>
                                    <input type="email" name="user_email" class="input-text full-width" value="" placeholder="enter your email" />
                                </div>
                                <div class="form-group">
                                    <h4 class="title">@lang('hotel.write_review_section.review')</h4>
                                    <textarea name="comment" class="input-text full-width" placeholder="enter your review (minimum 200 characters)" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <h4 class="title">@lang('hotel.write_review_section.rate')</h4>
                                    <div class="overall-rating">
                                        <div class="star-rating clearfix">
                                            <div class="each-rating">
                                                <div class="five-stars-container editable-rating" data-original-stars="4">  
                                                </div>
                                                <input type="hidden" name="rating" value="4" id="rate">
                                            </div>
                                        </div>
                                    </div>
                              </div>
                                <!-- <div class="form-group">
                                    <h4 class="title">Share with friends <small>(Optional)</small></h4>
                                    <p>Share your review with your friends on different social media networks.</p>
                                    <ul class="social-icons icon-circle clearfix">
                                        <li class="twitter"><a title="Twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                        <li class="facebook"><a title="Facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                        <li class="googleplus"><a title="GooglePlus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                        <li class="pinterest"><a title="Pinterest" href="#" data-toggle="tooltip"><i class="soap-icon-pinterest"></i></a></li>
                                    </ul>
                                </div> -->
                                <div class="form-group col-md-5 no-float no-padding no-margin">
                                    <button type="submit" class="btn-large full-width">SUBMIT REVIEW</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="sidebar col-md-3">
                <article class="detailed-logo">
                    <figure>
                        <img width="114" height="85" src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}" alt="">
                    </figure>
                    <div class="details">
                        <h2 class="box-title">{{$hotel->name}}<small><i class="soap-icon-departure yellow-color"></i><span class="fourty-space">{{$hotel->country->name}},{{$hotel->city->name}}</span></small></h2>
                        <span class="price clearfix">
                            <small class="pull-left">avg/night</small>
                            <span class="pull-right">${{$hotel->price}}</span>
                        </span>
                        <div class="feedback clearfix">
                            <div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: {{$hotel->getHotelRate($hotel->avg_rating)}}%;"></span></div>
                            <span class="review pull-right">{{$hotel->reviews_count}} reviews</span>
                        </div>
                        <p class="description">{{substr($hotel->description,0,200)}}.</p>
                    </div>
                </article>
                <div class="travelo-box">
                    <h4>Similar Listings</h4>
                    <div class="image-box style14">
                        <article class="box">
                            <figure>
                                <a href="#"><img src="http://placehold.it/63x59" alt="" /></a>
                            </figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Plaza Tour Eiffel</a></h5>
                                <label class="price-wrapper">
                                    <span class="price-per-unit">$170</span>avg/night
                                </label>
                            </div>
                        </article>
                        <article class="box">
                            <figure>
                                <a href="#"><img src="http://placehold.it/63x59" alt="" /></a>
                            </figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Sultan Gardens</a></h5>
                                <label class="price-wrapper">
                                    <span class="price-per-unit">$620</span>avg/night
                                </label>
                            </div>
                        </article>
                        <article class="box">
                            <figure>
                                <a href="#"><img src="http://placehold.it/63x59" alt="" /></a>
                            </figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">Park Central</a></h5>
                                <label class="price-wrapper">
                                    <span class="price-per-unit">$322</span>avg/night
                                </label>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="travelo-box book-with-us-box">
                    <h4>Why Book with us?</h4>
                    <ul>
                        <li>
                            <i class="soap-icon-hotel-1 circle"></i>
                            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-savings circle"></i>
                            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-support circle"></i>
                            <h5 class="title"><a href="#">Excellent Support</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
    <script>

      function initMap() {
        var myLatLng  = {lat : {{$hotel->latitude}} , lng:{{$hotel->longitude}}};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatLng
          });
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!',
          });

      }
    </script>





    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0fOlryatxjNt1vQMdT0oKxaR9HI8zww&libraries=places&callback=initMap"></script>

    <script type="text/javascript" src="js/gmap3.min.js"></script>

<script type="text/javascript">
    tjq(document).ready(function() {
                        
            // editable rating
            tjq(".editable-rating.five-stars-container").each(function() {
                var oringnal_value = tjq(this).data("original-stars");
                if (typeof oringnal_value == "undefined") {
                    oringnal_value = 0;
                } else {
                    //oringnal_value = 10 * parseInt(oringnal_value);
                }

                tjq(this).slider({
                    range: "min",
                    value: oringnal_value,
                    min: 0,
                    max: 5,
                    slide: function( event, ui ) {
                        console.log(ui.value);
                        tjq("#rate").val(ui.value);

                    }
                });
            });
        });
        
</script>
<script type="text/javascript">
    tjq(document).ready(function(){
        var page = 2;
        tjq('#load_more_reviews').on('click',function(){
                tjq.ajax({
                  url: "{{route('more_reviews')}}",
                  type: "get", //send it through get method
                  data: { 
                    page: page, 
                    hotel_id: '{{$hotel->id}}', 
                  },
                  success: function(response) {
                    //Do Something
                    page++;

                    if(response.length > 0 )
                    {
                        tjq('.guest-reviews').append(response);
                    }
                  },
                  error: function(xhr) {
                    //Do Something to handle error
                  }
                });

        });

        


    });
</script>

@endsection