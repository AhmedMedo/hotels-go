                          <div class="hotel-list">
                              <div class="row image-box listing-style2">
                                @if(count($hotels))
                                @foreach($hotels as $hotel)
                                  <div class="col-sms-6 col-sm-6 col-md-4">
                                      <article class="box">
                                          <figure>
                                              <a title="" href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img width="270" height="160" alt="" src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}"></a>
                                          </figure>
                                          <div class="details">
                                              <a title="View all" href="{{route('hotels.show',$hotel)}}" class="pull-right button uppercase">select</a>
                                              <h4 class="box-title">{{$hotel->name}}</h4>
                                              <label class="price-wrapper">
                                                  <span class="price-per-unit">${{$hotel->price}}</span>avg/night
                                              </label>
                                          </div>
                                      </article>
                                  </div>
                                  @endforeach
                                  @endif
                              </div>
                          </div>