<div class="hotel-list listing-style3 hotel">
    <ul class="list">
    @if(count($hotels))
    @foreach($hotels as $hotel)
        <li>
        <article class="box">
            <figure class="col-sm-5 col-md-4">
                <a title="" href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img width="270" height="160" alt="" src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}"></a>
            </figure>
            <div class="details col-sm-7 col-md-8">
                <div>
                    <div>
                        <h4 class="box-title name">{{$hotel->name}}<small><i class="soap-icon-departure yellow-color"></i> {{$hotel->country->name}}, {{$hotel->city->name}}</small></h4>
                        @if(count($hotel->amenties))
                        <div class="amenities">
                            @foreach($hotel->amenties()->take(4)->get() as $amenty)
                                <i class="{{$amenty->soap_icon}} circle"></i>
                            @endforeach
                        </div>
                        @endif
                    </div>
                    <div>
                        <div class="five-stars-container">
                            <span class="five-stars" style="width: {{$hotel->getHotelRate($hotel->avg_rating)}}%;"></span>
                            <div class="rating" style="display: none;">{{$hotel->avg_rating}} </div>
                        </div>
                        <span class="review">{{$hotel->reviews_count}} reviews</span>
                    </div>
                </div>
                <div>
                    <p>{{ $hotel->description}}.</p>
                    <div>
                        <span class="price"><small>AVG/NIGHT</small>${{$hotel->price}}</span>
                        <a class="button btn-small full-width text-center" title="" href="{{route('hotels.show',$hotel)}}">SELECT</a>
                    </div>
                </div>
            </div>
        </article>
    </li>
    @endforeach

    @endif
    </ul>
</div>