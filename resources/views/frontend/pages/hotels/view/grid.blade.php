<div class="hotel-list">
        <div class="row image-box hotel listing-style1">
            <ul class="list">
        	@if(count($hotels))
        	@foreach($hotels as $hotel)
            <li>
                <div class="col-sm-6 col-md-4">
                    <article class="box">
                        <figure>
                        	<a title="" href="{{route('hotels.slider',$hotel->id)}}" class="hover-effect popup-gallery"><img width="270" height="160" alt="" src="{{asset(env('UPLOAD_PATH'))}}/{{$hotel->featured_image}}"></a>
                        </figure>
                        <div class="details">
                            <span class="price">
                                <small>avg/night</small>
                                ${{$hotel->price}}
                            </span>
                            <h4 class="box-title name">{{$hotel->name}}<small>{{$hotel->country->name}} {{$hotel->city->name}}</small></h4>
                            <div class="feedback">
                                <div data-placement="bottom" data-toggle="tooltip" class="five-stars-container" title="4 stars"><span style="width: {{$hotel->getHotelRate($hotel->avg_rating)}}%;" class="five-stars"></span></div>
                                <div class="rating" style="display: none;">{{$hotel->avg_rating}} </div>

                                <span class="review">{{$hotel->reviews_count}} reviews</span>
                            </div>
                            <p class="description">{{ substr($hotel->description,0,20)}}</p>
                            <div class="action">
                                <a class="button btn-small" href="{{route('hotels.show',$hotel)}}">SELECT</a>
                                <a class="button btn-small yellow popup-map" href="#" data-box="{{$hotel->latitude}}, {{$hotel->longitude}}">VIEW ON MAP</a>
                            </div>
                        </div>
                    </article>
                </div>
            </li>
            @endforeach
            @endif
            </ul>

        </div>
</div>