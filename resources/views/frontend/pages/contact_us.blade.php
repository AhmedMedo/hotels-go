@extends('frontend.layout.main')
@section('styles')
    
@endsection
@section('content')
<div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Contact Us</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{route('homepage')}}">HOME</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="travelo-google-map block"></div>
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="travelo-box contact-us-box">
                        <h4>Contact us</h4>
                        <ul class="contact-address">
                            <li class="address">
                                <i class="soap-icon-address circle"></i>
                                <h5 class="title">Address</h5>
                                <p>United States of America.</p>
                                <p>P.O Box,  353 Three Avenue.</p>
                            </li>
                            <li class="phone">
                                <i class="soap-icon-phone circle"></i>
                                <h5 class="title">Phone</h5>
                                <p>Local: 1-800-123-hello</p>
                                <p>Mobile: 1-800-123-hello</p>
                            </li>
                            <li class="email">
                                <i class="soap-icon-message circle"></i>
                                <h5 class="title">Email</h5>
                                <p>info@hotelsgo.co</p>
                                <p>www.hotelsgo.com</p>
                            </li>
                        </ul>
                        <ul class="social-icons full-width">
                            <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="soap-icon-twitter"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="GooglePlus"><i class="soap-icon-googleplus"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="soap-icon-facebook"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Linkedin"><i class="soap-icon-linkedin"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Vimeo"><i class="soap-icon-vimeo"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="travelo-box">
                        <form class="contact-form" action="{{route('contact_us_submit')}}" method="post" id="contactform">
                            @csrf
                            <h4 class="box-title">Send us a Message</h4>
                            <div class="alert small-box" style="display: none;"></div>
                            <div class="row form-group">
                                <div class="col-xs-6">
                                    <label>Your Name</label>
                                    <input type="text" name="name" class="input-text full-width">
                                </div>
                                <div class="col-xs-6">
                                    <label>Your Email</label>
                                    <input type="text" name="email" class="input-text full-width">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Your Message</label>
                                <textarea name="message" rows="6" class="input-text full-width" placeholder="write message here"></textarea>
                            </div>
                            <button type="submit" class="btn-large full-width" id="submit">SEND MESSAGE</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0fOlryatxjNt1vQMdT0oKxaR9HI8zww&libraries=places&callback=initMap"></script>

<script type="text/javascript" src="{{asset('frontend/js/gmap3.min.js')}}"></script>
<script type="text/javascript">
    tjq(".travelo-google-map").gmap3({
        map: {
            options: {
                center: [48.85661, 2.35222],
                zoom: 12
            }
        },
        marker:{
            values: [
                {latLng:[48.85661, 2.35222], data:"Paris"}

            ],
            options: {
                draggable: false
            },
        }
    });
</script>
@endsection

@section('scripts')

<script type="text/javascript">


</script>


@endsection