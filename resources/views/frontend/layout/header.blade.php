        <header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
<!--                         <li><a href="#">My Account</a></li>
 -->                        <li class="ribbon">
                            @if(App::getLocale()=="en")
                            <a href="#">English</a>
                            @else
                            <a href="#">Arabic</a>
                            @endif
                            <ul class="menu mini">
                                @if(App::getLocale()=="en")
                                <li><a href="{{LaravelLocalization::getLocalizedURL("ar")}}" title="Arabic">Arabic</a></li>
                                @else
                                <li><a href="{{LaravelLocalization::getLocalizedURL("en")}}" title="Arabic">English</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
            <!--         <ul class="quick-menu pull-right">
                        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency">
                            <a href="#" title="">USD</a>
                            <ul class="menu mini">
                                <li><a href="#" title="AUD">AUD</a></li>
                                <li><a href="#" title="BRL">BRL</a></li>
                                <li class="active"><a href="#" title="USD">USD</a></li>
                                <li><a href="#" title="CAD">CAD</a></li>
                                <li><a href="#" title="CHF">CHF</a></li>
                                <li><a href="#" title="CNY">CNY</a></li>
                                <li><a href="#" title="CZK">CZK</a></li>
                                <li><a href="#" title="DKK">DKK</a></li>
                                <li><a href="#" title="EUR">EUR</a></li>
                                <li><a href="#" title="GBP">GBP</a></li>
                                <li><a href="#" title="HKD">HKD</a></li>
                                <li><a href="#" title="HUF">HUF</a></li>
                                <li><a href="#" title="IDR">IDR</a></li>
                            </ul>
                        </li>
                    </ul> -->
                </div>
            </div>
            
            <div class="main-header">
                
                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>
                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="{{route('homepage')}}" title="Hotels GO - home">
                            <img src="{{asset('frontend/images/new_logo.png')}}" alt="Hotels GO" />
                        </a>
                    </h1>

                    <nav id="main-menu" role="navigation">
                        <ul class="menu">

                            <li class="menu-item-has-children {{request()->segment(2) == '' ? 'active':''}}">
                                <a href="{{route('homepage')}}">@lang('global.menu.home')</a>
                            </li>
                            <li class="menu-item-has-children {{request()->segment(2) == 'hotels' ? 'active':''}}">
                                <a href="{{route('hotels.list')}}">@lang('global.menu.hotels')</a>
                            </li>
                            <li class="menu-item-has-children {{request()->segment(2) == 'about-us' ? 'active':''}}">
                                <a href="{{route('about_us')}}">@lang('global.menu.about_us')</a>
                            </li>
                            <li class="menu-item-has-children {{request()->segment(2) == 'contact-us' ? 'active':''}}">
                                <a href="{{route('contact_us')}}">@lang('global.menu.contact_us')</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="{{route('homepage')}}">Home</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="{{route('hotels.list','list')}}">Hotels</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="hotel-index.html">About Us</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="hotel-index.html">Contact Us</a>
                        </li>
                    </ul>
                    <ul class="mobile-topnav container">
                        <li class="ribbon language menu-color-skin">
                            <a href="#" data-toggle="collapse">ENGLISH</a>
                            <ul class="menu mini">
                                <li><a href="#" title="Dansk">Arabic</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                </nav>
            </div>

        </header>
