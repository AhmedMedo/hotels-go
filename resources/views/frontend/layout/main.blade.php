<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
	@include('frontend.partials.head')
</head>
<body>
    <div id="page-wrapper">
    	@include('frontend.layout.header')

    		@yield('content')

    	@include('frontend.layout.footer')
    </div>
@include('frontend.partials.javascripts')
</body>
</html>