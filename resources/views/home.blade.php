@extends('layouts.app')
@section('styles')
<style type="text/css">
    .zoom:hover {
  transform: scale(1.05); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>

@endsection
@section('content')
    <!-- -->

    <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{$reviews_count}}</h3>

                  <p>Review Added</p>
                </div>
                <div class="icon">
                  <i class="ion-ios-chatbubble-outline"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{$hotels_count}}</h3>

                  <p>Hotel Added</p>
                </div>
                <div class="icon">
                  <i class="fa  fa-hotel"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>65</h3>

                  <p>Unique Visitors</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>

    <!-- -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('global.app_dashboard')</div>

                <div class="panel-body">
                    <form action="{{ route('admin.home') }}">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                <input type="text" name="query" class="form-control" placeholder="Search by name" value="{{ request('query', '') }}" />
                                </div>
                             </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                <select name="category_id" class="form-control">
                                    <option value="0">-- search by category --</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"
                                                @if (request('category_id', 0) == $category->id) selected @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <select name="tag_id" class="form-control">
                                <option value="0">-- search by tag --</option>
                                @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}"
                                            @if (request('tag_id', 0) == $tag->id) selected @endif>{{ $tag->name }}</option>
                                @endforeach
                            </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                 </div>
                            </div>  
                    </div>
                    </form>
                    <hr />

                    @foreach ($products->chunk(4) as $chunk)
                        <div class="row">
                            @foreach($chunk as $product)
<!--                             <div class="col-md-2">
                                @if ($product->photo1 != '')
                                <img src="/{{ $product->photo1 }}" width="100" />
                                @else 
                                @endif
                            </div>
                            <div class="col-md-8">
                                <a href="{{ route('admin.products.show', $product->id) }}">{{ $product->name }}</a>
                                <br />
                                {{ $product->description }}
                            </div>
                            <div class="col-md-2">
                                @if ($product->reviews_count > 0)
                                {{ number_format($product->avg_rating, 2) }} / 5.00
                                <br />
                                {{ $product->reviews_count }} votes
                                @else
                                    No ratings yet.
                                @endif
                            </div> -->
                             <div class="col-md-3 zoom">
                                <div class="box box-solid box-primary">
                                 <div class="box-header">
                                        <h3 class="box-title"> <a href="{{ route('admin.products.show', $product->id) }}" >{{ $product->name }}</a></h3>
                                 </div><!-- /.box-header -->
                                     <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12"> 
                                                @if ($product->photo1 != '')
                                                <img  src="{{asset('')}}{{ $product->photo1 }}" height="142px" width="100%" />
                                                @else
                                              <img src="{{asset('images/hotel-hc.svg')}}" height="142px" width="100%" alt="Photo">
                                              @endif
                                            </div>
                                          
                                        </div>
                                            <!--    <div class="row">
                                            <div class="col-md-12"> 
                                            {{ $product->description }}
                                             </div>
                                        </div> -->
                                        <div class="row">
                                            @if ($product->reviews_count > 0)
                                            <div class="col-md-3">
                                                <h4><span class="label label-success">{{ number_format($product->avg_rating, 2) }} / 5.00
                                                </span></h4>
                                               
                                            </div>
                                            <div class="col-md-3">
                                                <h4><span class="label label-success">{{ $product->reviews_count }} votes </span></h4> 

                                            </div>

                                            @else
                                                <div class="col-md-12">
                                                <h4><span class="label label-danger">No Rating</span></h4>
                                             </div>
                                            @endif
                                        </div>
                                           <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{ route('admin.products.show', $product->id) }}" class="btn btn-info pull-right">Show</a>
                                                </div>
                                            </div>
                                    </div>

                                    <!-- /.box-body -->
                                </div>
                             </div>
                            @endforeach

                        </div>
                        <hr />
                    @endforeach
                    <div class="text-center">
                        {{ $products->links() }}
                     </div>

                </div>
            </div>
        </div>
    </div>
@endsection
