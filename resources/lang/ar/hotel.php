<?php
    return [
        'hotel_deatiled'=>'تفاصيل الفندق',
        'photos'=>'الصور',
        'map'=>'الموقع',
        'view'=>'الرابط',
        'description'=>'الوصف',
        'amenities'=>'الرفاهيات',
        'reviews'=>'التقييمات',
        'write_review'=>'كتابة تقييم',
        'description_section'=>[
            'hotel_type'=>'نوع الفندق',
            'extra_people'=>'اشخاص اخرون',
            'minimum_stay'=>'الحد الأدني للإقامة',
            'security_deposit'=>'مبلغ التأمين',
            'country'=>'الدولة',
            'city'=>'المدينة'

        ],
        'reviews_section'=>[
			'service'	=> 'الخدمة',
			'sleep_quality'	=> 'جودة النوم',
			'location'	=> 'الموقع',
			'swimming_pool'	=> 'حمام السباحة',
			'value'	=> 'القيمة',
			'cleanliness'	=> 'النظافة',
			'rooms'	=> 'الغرف',
			'fitness_facilty'	=> 'تسهيلات الالعاب الرياضية',
        ],
        'guest_reviews'=>'تقييمات الزوار',
        'write_review_section'=>[
            'title'=>'اكتب عنوان لتقييمك',
            'name'=>'اسمك',
            'email'=>'بريدك الإليكتروني',
            'review'=>'تقييمك',
            'rate'=>'التقييم',
            'submit'=>' حفظ'
        ]



    ];


?>