<?php
    return [
        'hotel_deatiled'=>'Hotel Detailed',
        'photos'=>'Photos',
        'map'=>'Map',
        'view'=>'View',
        'description'=>'Description',
        'amenities'=>'Amenities',
        'reviews'=>'Reviews',
        'write_review'=>'Write a Review',
        'description_section'=>[
            'hotel_type'=>'hotel type',
            'extra_people'=>'extra people',
            'minimum_stay'=>'Minimum Stay',
            'security_deposit'=>'Security Deposit',
            'country'=>'country',
            'city'=>'city'

        ],
        'reviews_section'=>[
			'service'	=> 'Service',
			'sleep_quality'	=> 'Sleep Quality',
			'location'	=> 'Location',
			'swimming_pool'	=> 'Swimming Pool',
			'value'	=> 'Value',
			'cleanliness'	=> 'Cleanliness',
			'rooms'	=> 'Rooms',
			'fitness_facilty'	=> 'Fitness Facilty',
        ],
        'guest_reviews'=>'Guest Reviews',
        'write_review_section'=>[
            'title'=>'Title of your review',
            'name'=>'Your Name',
            'email'=>'Your Email',
            'review'=>'Your Review',
            'rate'=>'Your Rate',
            'submit'=>' Submit Review'
        ]



    ];


?>