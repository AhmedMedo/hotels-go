<?php 
	return [

		'search'=>[
			'your_destination'=>'Your destination',
			'text_placeholder'=>'enter a destination or hotel name',
			'price'=>'Price',
			'rate'=>'Rate',
			'rooms'=>'Rooms',
			'adults'=>'Adults',
			'kids'=>'Kids',
			'search_now'=>'SEARCH NOW',
		],
		'recommended_hotels'=>'Recommended Hotels',
		'top_reviewers'=>'Top Reviewers',
		'hot_offers'=>'Hot Offers',
		'hot_offer'=>'Hot Offer',
		'subscribe'=>[
			'subscribe_title'=>'Subscribe with us to get last news.',
			'hotels_and_resorts'=>'Hotel and Resorts Available!',
			'button'=>'Subscribe',
			'email_placeholder'=>'Enter your email'
		],
		'footer_about'=>'Hotel Go has been created to help you search and reach the recommendations & ratings of other travelers around the world where you can find the right hotel for you by the timing of your trip and budget with ease.',
		'about'=>'About Hotels GO',
		'discover'=>'Discover'

	];


 ?>