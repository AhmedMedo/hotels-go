<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProductsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('city_id')->nullable();
            $table->renameColumn('location_id', 'country_id');
            $table->integer('num_of_rooms')->default(0);
            $table->integer('adults')->default(0);
            $table->integer('num_of_kids')->default(0);
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->renameColumn('photo3', 'featured_image');
            $table->float('service_rate')->default(0);
            $table->float('sleep_quality_rate')->default(0);
            $table->float('localtion_rate')->default(0);
            $table->float('value_rate')->default(0);
            $table->float('cleanliness_rate')->default(0);
            $table->float('rooms_rate')->default(0);
            $table->float('fitness_facilty_rate')->default(0);
            $table->tinyInteger('published')->default(1);





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
