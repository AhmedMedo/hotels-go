<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelAmentiesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('hotel_amenties')) {
            Schema::create('hotel_amenties', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('hotel_id')->unsigned()->nullable();
                $table->integer('amenity_id')->unsigned()->nullable();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
