<?php

use Illuminate\Database\Seeder;
use App\Amenity;
class AmenitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $items =[

        	['name'=>'WI_FI','soap_icon' =>'soap-icon-wifi'],
        	['name'=>'swimming pool','soap_icon' =>'soap-icon-swimming'],
        	['name'=>'television','soap_icon' =>'soap-icon-television'],
        	['name'=>'coffee','soap_icon' =>'soap-icon-coffee'],
        	['name'=>'air conditioning','soap_icon' =>'soap-icon-aircon'],
        	['name'=>'fitness facility','soap_icon' =>'soap-icon-fitnessfacilit'],
        	['name'=>'fridge','soap_icon' =>'soap-icon-fridge'],
        	['name'=>'wine bar','soap_icon' =>'soap-icon-winebar'],
        	['name'=>'smoking allowed','soap_icon' =>'soap-icon-smoking'],
        	['name'=>'entertainment','soap_icon' =>'soap-icon-entertainment'],
        	['name'=>'secure vault','soap_icon' =>'soap-icon-securevault'],
        	['name'=>'pick and drop','soap_icon' =>'soap-icon-pickanddrop'],
        	['name'=>'room service','soap_icon' =>'soap-icon-pets'],
        	['name'=>'pets allowed','soap_icon' =>'soap-icon-pets'],
        	['name'=>'play place','soap_icon' =>'soap-icon-playplace'],
        	['name'=>'complimentary breakfast','soap_icon' =>'soap-icon-breakfast'],
        	['name'=>'Free parking','soap_icon' =>'soap-icon-parking'],
        	['name'=>'conference room','soap_icon' =>'soap-icon-conference'],
        	['name'=>'fire place','soap_icon' =>'soap-icon-fireplace'],
        	['name'=>'Handicap Accessible','soap_icon' =>'soap-icon-handicapaccessiable'],
        	['name'=>'Doorman','soap_icon' =>'soap-icon-doorman'],
        	['name'=>'Hot Tub','soap_icon' =>'soap-icon-tub'],
        	['name'=>'Elevator in Building','soap_icon' =>'soap-icon-elevator'],
        	['name'=>'Suitable for Events','soap_icon' =>'soap-icon-star']

        	];

        	foreach ($items as $item) {
        	    Amenity::create($item);
        	}


    }
}
